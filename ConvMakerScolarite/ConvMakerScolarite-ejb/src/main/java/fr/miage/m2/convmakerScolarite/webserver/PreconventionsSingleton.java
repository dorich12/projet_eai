package fr.miage.m2.convmakerScolarite.webserver;

import java.util.ArrayList;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import fr.miage.m2.convmakershared.Preconvention;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Topic;

/**
 *
 */
@Singleton
@LocalBean
public class PreconventionsSingleton {
    
    @Resource(lookup = "preconv")
    private Topic topic;
    
    @Inject
    private JMSContext context;
    
    @EJB
    EtudiantsSingleton etudiants;
    
    private final ArrayList<Preconvention> preconventions;

    public PreconventionsSingleton() {
        preconventions = new ArrayList<>();
    }

    /**
     * Récupère l'ensemble des préconventions dans la liste en mémoire
     * @return liste de préconvention
     */
    public ArrayList<Preconvention> getPreconventions() {
        return preconventions;
    }
    
    /**
     * Récupère dans la liste en mémoire la préconvention d'id passé en paramètre
     * @param id
     * @return preconvention ou null
     */
    public Preconvention getPreconvention(int id) {
        return this.rechercher(id);
    }
    
    /**
     * Modifier la préconvention de même id dans la liste en mémoire
     * @param newPc
     * @return preconvention modifiée
     */
    public Preconvention editPreconvention(Preconvention newPc) {
        // On vérifie qu'il n'y a pas eu une modification concurrence
        // avec les deux autres services
        if (newPc.getNoPC() < preconventions.size()) {
            Preconvention PcActuelle = preconventions.get(newPc.getNoPC());
            if (PcActuelle.getValidationJuridique() != newPc.getValidationJuridique()) {
                newPc.setValidationJuridique(PcActuelle.getValidationJuridique());
            }
            if (PcActuelle.getValidationPedagogique()!= newPc.getValidationPedagogique()) {
                newPc.setValidationPedagogique(PcActuelle.getValidationPedagogique());
            }
        }
        
        // On met à jour la preConvention
        miseAJourPreconvention(newPc);
        
        // On renvoi la préconvention dans le topic
         System.out.println("Envoi de scolarite");
        try {
            ObjectMessage om = context.createObjectMessage(newPc);
            om.setJMSType(newPc.getEtatPC().toString());
            context.createProducer().send(topic, om);
        } catch (JMSException ex) {
            Logger.getLogger(PreconventionsSingleton.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return getPreconvention(newPc.getNoPC());
    }
    
    /**
     * Méthode apellée à chaque réception de préconvention par JMS
     * Met à jour la liste en mémoire
     * @param newPc 
     */
    public void miseAJourPreconvention(Preconvention newPc) {
        Preconvention preFind = this.rechercher(newPc.getNoPC());
                
        if (preFind != null) {
            // On remplace l'object car plusieurs attributs
            // peuvent être modifiés, on veux la dernière version
            preconventions.set(preconventions.indexOf(preFind), newPc);
        } else {
            preconventions.add(newPc);
        }
    }
    
    /**
     * Recherche dans la liste en mémoire la préconvention d'id passé en paramètre
     * @param noPc
     * @return preconvention ou null
     */
    private Preconvention rechercher(int noPc) {
        for (Preconvention p: this.preconventions) {
            if (p.getNoPC() == noPc) {
                return p;
            }
        }
        
        return null;
    }
}