/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.miage.m2.convmakerScolarite.messages;

import fr.miage.m2.convmakershared.Etudiant;
import java.io.Serializable;

/**
 *
 */
public class Inscrire implements Serializable {
    
    private int Annee;
    
    private Formation formation;
    
    private Etudiant etudiant;

    public Inscrire(int Annee, Formation formation, Etudiant etudiant) {
        this.Annee = Annee;
        this.formation = formation;
        this.etudiant = etudiant;
    }
    
    public Inscrire(){
        
    }

    public int getAnnee() {
        return Annee;
    }

    public void setAnnee(int Annee) {
        this.Annee = Annee;
    }

    public Formation getFormation() {
        return formation;
    }

    public void setFormation(Formation formation) {
        this.formation = formation;
    }

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }

    @Override
    public String toString() {
        return "Inscrire{" + "Annee=" + Annee + ", formation=" + formation + ", etudiant=" + etudiant + '}';
    }

}
