package fr.miage.m2.convmakerScolarite.rest;

import fr.miage.m2.convmakershared.Preconvention;
import fr.miage.m2.convmakerScolarite.webserver.PreconventionsSingleton;
import java.util.List;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * REST Web Service
 *
 */
@Path("PreConvention")
public class PreconventionsRessource {
    
    PreconventionsSingleton preconventionsSingleton = lookupPreconventionsSingletonBean();

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PreconventionsRessource
     */
    public PreconventionsRessource() {
    }
    
    /**
     * GET récupère la liste des pré-conventions à traiter pour ce service
     * @return liste des préconventions
     */
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Preconvention> getPreConventions() {
        return preconventionsSingleton.getPreconventions();
    }

    /**
     * Récupère l'instance du bean PreconventionSingleton
     * @return PreconventionsSingleton bean
     */
    private PreconventionsSingleton lookupPreconventionsSingletonBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (PreconventionsSingleton) c.lookup("java:global/ConvMakerScolarite-ear/ConvMakerScolarite-ejb-1.0/PreconventionsSingleton!fr.miage.m2.convmakerScolarite.webserver.PreconventionsSingleton");
        } catch (NamingException ne) {
            throw new RuntimeException(ne);
        }
    }
    
}
