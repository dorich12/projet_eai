package fr.miage.m2.convmakerStages.webserver;

import fr.miage.m2.convmakershared.EtatPreconvention;
import java.util.ArrayList;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import fr.miage.m2.convmakershared.Preconvention;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Topic;

/**
 *
 */
@Singleton
@LocalBean
public class PreconventionsSingleton {
    
    @Resource(lookup = "preconv")
    private Topic topic;
    
    @Inject
    private JMSContext context;
    
    @EJB
    EtudiantsSingleton etudiants;
    
    private final ArrayList<Preconvention> preconventions;

    public PreconventionsSingleton() {
        preconventions = new ArrayList<>();
    }

    /**
     * Récupère l'ensemble des préconventions dans la liste en mémoire
     * @return liste de préconvention
     */
    public ArrayList<Preconvention> getPreconventions() {
        return preconventions;
    }
    
    /**
     * Récupère dans la liste en mémoire la préconvention d'id passé en paramètre
     * @param id
     * @return preconvention ou null
     */
    public Preconvention getPreconvention(int id) {
        return this.rechercher(id);
    }
    
    /**
     * Met à jour l'état de la préconvention en fonction des vérifications
     * @param newPc 
     */
    public void verifierEtat(Preconvention newPc) {
        boolean modified = false;
        
        // Vérification des validations
        if (newPc.getValidationAdministratif() == 1 &&
                newPc.getValidationJuridique() == 1 &&
                newPc.getValidationPedagogique() == 1) {
            // Passage à CONFORME
            newPc.setEtatPC(EtatPreconvention.CONFORME);
            modified = true;
        }
        if (newPc.getValidationAdministratif() == 2 ||
                newPc.getValidationJuridique() == 2 ||
                newPc.getValidationPedagogique() == 2) {
            // Passage à NON_CONFORME
            newPc.setEtatPC(EtatPreconvention.NON_CONFORME);
            modified = true;
        }

        if (modified) {
            // On met à jour la preConvention
            preconventions.set(preconventions.indexOf(newPc), newPc);
        
            // On renvoi la préconvention dans le topic en cas de modification
            System.out.println("Envoi preconv depuis Stages");
            try {
                ObjectMessage om = context.createObjectMessage(newPc);
                om.setJMSType(newPc.getEtatPC().toString());
                context.createProducer().send(topic, om);
            } catch (JMSException ex) {
                Logger.getLogger(PreconventionsSingleton.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * Méthode apellée à chaque réception de préconvention par JMS
     * Met à jour la liste en mémoire
     * @param newPc 
     */
    public void miseAJourPreconvention(Preconvention newPc) {
        Preconvention preFind = this.rechercher(newPc.getNoPC());
                
        if (preFind != null) {
            // On remplace l'object car plusieurs attributs
            // peuvent être modifiés, on veux la dernière version
            preconventions.set(preconventions.indexOf(preFind), newPc);
        } else {
            preconventions.add(newPc);
        }
        
        // Le service des stages doit vérifier les vérifications
        // et modifier le statut en fonction
        verifierEtat(newPc);
    }
    
    /**
     * Recherche dans la liste en mémoire la préconvention d'id passé en paramètre
     * @param noPc
     * @return preconvention ou null
     */
    private Preconvention rechercher(int noPc) {
        for (Preconvention p: this.preconventions) {
            if (p.getNoPC() == noPc) {
                return p;
            }
        }
        
        return null;
    }
}