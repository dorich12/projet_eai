/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.m2.convmakerDashboard.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author David
 */
@Entity
@Table(name = "ETUDIANT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Etudiant.findAll", query = "SELECT e FROM Etudiant e")
    , @NamedQuery(name = "Etudiant.findByNoEtudiant", query = "SELECT e FROM Etudiant e WHERE e.noEtudiant = :noEtudiant")
    , @NamedQuery(name = "Etudiant.findByNomEtudiant", query = "SELECT e FROM Etudiant e WHERE e.nomEtudiant = :nomEtudiant")
    , @NamedQuery(name = "Etudiant.findByPrenomEtudiant", query = "SELECT e FROM Etudiant e WHERE e.prenomEtudiant = :prenomEtudiant")})
public class Etudiant implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "noEtudiant")
    private Integer noEtudiant;
    @Size(max = 255)
    @Column(name = "nomEtudiant")
    private String nomEtudiant;
    @Size(max = 255)
    @Column(name = "prenomEtudiant")
    private String prenomEtudiant;
    @OneToMany(mappedBy = "etudiantPC")
    private Collection<Preconvention> preconventionCollection;

    public Etudiant() {
    }

    public Etudiant(Integer noEtudiant) {
        this.noEtudiant = noEtudiant;
    }

    public Integer getNoEtudiant() {
        return noEtudiant;
    }

    public void setNoEtudiant(Integer noEtudiant) {
        this.noEtudiant = noEtudiant;
    }

    public String getNomEtudiant() {
        return nomEtudiant;
    }

    public void setNomEtudiant(String nomEtudiant) {
        this.nomEtudiant = nomEtudiant;
    }

    public String getPrenomEtudiant() {
        return prenomEtudiant;
    }

    public void setPrenomEtudiant(String prenomEtudiant) {
        this.prenomEtudiant = prenomEtudiant;
    }

    @XmlTransient
    public Collection<Preconvention> getPreconventionCollection() {
        return preconventionCollection;
    }

    public void setPreconventionCollection(Collection<Preconvention> preconventionCollection) {
        this.preconventionCollection = preconventionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noEtudiant != null ? noEtudiant.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Etudiant)) {
            return false;
        }
        Etudiant other = (Etudiant) object;
        if ((this.noEtudiant == null && other.noEtudiant != null) || (this.noEtudiant != null && !this.noEtudiant.equals(other.noEtudiant))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.miage.m2.convmakerDashboard.entities.Etudiant[ noEtudiant=" + noEtudiant + " ]";
    }
    
}
