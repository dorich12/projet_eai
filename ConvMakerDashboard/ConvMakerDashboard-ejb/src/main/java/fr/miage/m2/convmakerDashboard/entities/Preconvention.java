/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.m2.convmakerDashboard.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David
 */
@Entity
@Table(name = "PRECONVENTION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Preconvention.findAll", query = "SELECT p FROM Preconvention p")
    , @NamedQuery(name = "Preconvention.findById", query = "SELECT p FROM Preconvention p WHERE p.id = :id")
    , @NamedQuery(name = "Preconvention.findByNoPC", query = "SELECT p FROM Preconvention p WHERE p.noPC = :noPC")
    , @NamedQuery(name = "Preconvention.findByDateDebutPC", query = "SELECT p FROM Preconvention p WHERE p.dateDebutPC = :dateDebutPC")
    , @NamedQuery(name = "Preconvention.findByDateFinPC", query = "SELECT p FROM Preconvention p WHERE p.dateFinPC = :dateFinPC")
    , @NamedQuery(name = "Preconvention.findByGratificationPC", query = "SELECT p FROM Preconvention p WHERE p.gratificationPC = :gratificationPC")
    , @NamedQuery(name = "Preconvention.findByEtatPC", query = "SELECT p FROM Preconvention p WHERE p.etatPC = :etatPC")
    , @NamedQuery(name = "Preconvention.findByNomEntreprisePC", query = "SELECT p FROM Preconvention p WHERE p.nomEntreprisePC = :nomEntreprisePC")
    , @NamedQuery(name = "Preconvention.findBySirenEntreprisePC", query = "SELECT p FROM Preconvention p WHERE p.sirenEntreprisePC = :sirenEntreprisePC")
    , @NamedQuery(name = "Preconvention.findByCodeAssurancePC", query = "SELECT p FROM Preconvention p WHERE p.codeAssurancePC = :codeAssurancePC")
    , @NamedQuery(name = "Preconvention.findByNomAssurancePC", query = "SELECT p FROM Preconvention p WHERE p.nomAssurancePC = :nomAssurancePC")
    , @NamedQuery(name = "Preconvention.findByNiveau", query = "SELECT p FROM Preconvention p WHERE p.niveau = :niveau")
    , @NamedQuery(name = "Preconvention.findByIntituleFormation", query = "SELECT p FROM Preconvention p WHERE p.intituleFormation = :intituleFormation")
    , @NamedQuery(name = "Preconvention.findByTuteurEnseignant", query = "SELECT p FROM Preconvention p WHERE p.tuteurEnseignant = :tuteurEnseignant")
    , @NamedQuery(name = "Preconvention.findByValidationJuridique", query = "SELECT p FROM Preconvention p WHERE p.validationJuridique = :validationJuridique")
    , @NamedQuery(name = "Preconvention.findByValidationPedagogique", query = "SELECT p FROM Preconvention p WHERE p.validationPedagogique = :validationPedagogique")
    , @NamedQuery(name = "Preconvention.findByValidationAdministratif", query = "SELECT p FROM Preconvention p WHERE p.validationAdministratif = :validationAdministratif")})
public class Preconvention implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NoPC")
    private int noPC;
    @Column(name = "dateDebutPC")
    @Temporal(TemporalType.DATE)
    private Date dateDebutPC;
    @Column(name = "dateFinPC")
    @Temporal(TemporalType.DATE)
    private Date dateFinPC;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "gratificationPC")
    private Float gratificationPC;
    @Lob
    @Size(max = 65535)
    @Column(name = "resumePC")
    private String resumePC;
    @Size(max = 255)
    @Column(name = "etatPC")
    private String etatPC;
    @Size(max = 255)
    @Column(name = "nomEntreprisePC")
    private String nomEntreprisePC;
    @Column(name = "sirenEntreprisePC")
    private Integer sirenEntreprisePC;
    @Column(name = "codeAssurancePC")
    private Integer codeAssurancePC;
    @Size(max = 255)
    @Column(name = "nomAssurancePC")
    private String nomAssurancePC;
    @Size(max = 255)
    @Column(name = "niveau")
    private String niveau;
    @Size(max = 255)
    @Column(name = "intituleFormation")
    private String intituleFormation;
    @Size(max = 255)
    @Column(name = "tuteurEnseignant")
    private String tuteurEnseignant;
    @Lob
    @Size(max = 65535)
    @Column(name = "raisonsRefus")
    private String raisonsRefus;
    @Column(name = "validationJuridique")
    private Integer validationJuridique;
    @Column(name = "validationPedagogique")
    private Integer validationPedagogique;
    @Column(name = "validationAdministratif")
    private Integer validationAdministratif;
    @JoinColumn(name = "etudiantPC", referencedColumnName = "noEtudiant")
    @ManyToOne
    private Etudiant etudiantPC;

    public Preconvention() {
    }

    public Preconvention(Integer id) {
        this.id = id;
    }

    public Preconvention(Integer id, int noPC) {
        this.id = id;
        this.noPC = noPC;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getNoPC() {
        return noPC;
    }

    public void setNoPC(int noPC) {
        this.noPC = noPC;
    }

    public Date getDateDebutPC() {
        return dateDebutPC;
    }

    public void setDateDebutPC(Date dateDebutPC) {
        this.dateDebutPC = dateDebutPC;
    }

    public Date getDateFinPC() {
        return dateFinPC;
    }

    public void setDateFinPC(Date dateFinPC) {
        this.dateFinPC = dateFinPC;
    }

    public Float getGratificationPC() {
        return gratificationPC;
    }

    public void setGratificationPC(Float gratificationPC) {
        this.gratificationPC = gratificationPC;
    }

    public String getResumePC() {
        return resumePC;
    }

    public void setResumePC(String resumePC) {
        this.resumePC = resumePC;
    }

    public String getEtatPC() {
        return etatPC;
    }

    public void setEtatPC(String etatPC) {
        this.etatPC = etatPC;
    }

    public String getNomEntreprisePC() {
        return nomEntreprisePC;
    }

    public void setNomEntreprisePC(String nomEntreprisePC) {
        this.nomEntreprisePC = nomEntreprisePC;
    }

    public Integer getSirenEntreprisePC() {
        return sirenEntreprisePC;
    }

    public void setSirenEntreprisePC(Integer sirenEntreprisePC) {
        this.sirenEntreprisePC = sirenEntreprisePC;
    }

    public Integer getCodeAssurancePC() {
        return codeAssurancePC;
    }

    public void setCodeAssurancePC(Integer codeAssurancePC) {
        this.codeAssurancePC = codeAssurancePC;
    }

    public String getNomAssurancePC() {
        return nomAssurancePC;
    }

    public void setNomAssurancePC(String nomAssurancePC) {
        this.nomAssurancePC = nomAssurancePC;
    }

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public String getIntituleFormation() {
        return intituleFormation;
    }

    public void setIntituleFormation(String intituleFormation) {
        this.intituleFormation = intituleFormation;
    }

    public String getTuteurEnseignant() {
        return tuteurEnseignant;
    }

    public void setTuteurEnseignant(String tuteurEnseignant) {
        this.tuteurEnseignant = tuteurEnseignant;
    }

    public String getRaisonsRefus() {
        return raisonsRefus;
    }

    public void setRaisonsRefus(String raisonsRefus) {
        this.raisonsRefus = raisonsRefus;
    }

    public Integer getValidationJuridique() {
        return validationJuridique;
    }

    public void setValidationJuridique(Integer validationJuridique) {
        this.validationJuridique = validationJuridique;
    }

    public Integer getValidationPedagogique() {
        return validationPedagogique;
    }

    public void setValidationPedagogique(Integer validationPedagogique) {
        this.validationPedagogique = validationPedagogique;
    }

    public Integer getValidationAdministratif() {
        return validationAdministratif;
    }

    public void setValidationAdministratif(Integer validationAdministratif) {
        this.validationAdministratif = validationAdministratif;
    }

    public Etudiant getEtudiantPC() {
        return etudiantPC;
    }

    public void setEtudiantPC(Etudiant etudiantPC) {
        this.etudiantPC = etudiantPC;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Preconvention)) {
            return false;
        }
        Preconvention other = (Preconvention) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.miage.m2.convmakerDashboard.entities.Preconvention[ id=" + id + " ]";
    }
    
}
