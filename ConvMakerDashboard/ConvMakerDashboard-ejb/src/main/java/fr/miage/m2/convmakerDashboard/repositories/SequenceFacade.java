/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.m2.convmakerDashboard.repositories;

import fr.miage.m2.convmakerDashboard.entities.Sequence;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author David
 */
@Stateless
public class SequenceFacade extends AbstractFacade<Sequence> implements SequenceFacadeLocal {

    @PersistenceContext(unitName = "fr.miage.m2_ConvMakerDashboard-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SequenceFacade() {
        super(Sequence.class);
    }
    
}
