package fr.miage.m2.convmakerDashboard.webserver;

import fr.miage.m2.convmakerDashboard.entities.Etudiant;
import fr.miage.m2.convmakerDashboard.entities.Preconvention;
import fr.miage.m2.convmakerDashboard.repositories.EtudiantFacadeLocal;
import fr.miage.m2.convmakerDashboard.repositories.PreconventionFacadeLocal;
import fr.miage.m2.convmakerDashboard.transforms.PreconventionTransform;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Topic;

/**
 *
 */
@Stateless
public class PreconventionsSingleton {
    
    @Resource(lookup = "preconv")
    private Topic topic;
    
    @Inject
    private JMSContext context;
    
    @EJB
    private EtudiantFacadeLocal etudiantFacade;
    
    @EJB
    private PreconventionFacadeLocal preconventionFacade;

    public PreconventionsSingleton() {
    }

    /**
     * Récupère la liste de toutes les préconventions du service
     * @return liste de préconventions
     */
    public List<fr.miage.m2.convmakershared.Preconvention> getPreconventions() {
        // Récupération dans la base de données à partir de la facade
        List<Preconvention> preFinds = preconventionFacade.findAll();
        List<fr.miage.m2.convmakershared.Preconvention> preRet = new ArrayList<>();
        
        for (Preconvention pc: preFinds) {
            // On transforme l'objet en ressource
            preRet.add(PreconventionTransform.entityToResource(pc));
        }
        
        return preRet;
    }
    
    /**
     * Récupère la préconvention à partir de l'id
     * @param id
     * @return preconvention
     */
    public fr.miage.m2.convmakershared.Preconvention getPreconvention(int id) {
        // Récupère la préconvention dans la base de données
        Preconvention preFind = preconventionFacade.findByNoPc(id);
        
        if (preFind != null) {
            // On transforme l'objet en ressource
            return PreconventionTransform.entityToResource(preFind);
        }
        
        return null;
    }
    
    /**
     * Méthode apellée à chaque nouvelle preconvention reçue en JMS
     * Met à jour la base de données
     * @param newPc 
     */
    public void miseAJourPreconvention(fr.miage.m2.convmakershared.Preconvention newPc) {
        // On vérfie si la préconvention est déjà dans la base de données
        Preconvention preFind = preconventionFacade.findByNoPc(newPc.getNoPC());
        
        if (preFind != null) {
            // Modification des attributs
            preFind.setEtatPC(newPc.getEtatPC().toString());
            preFind.setValidationAdministratif(newPc.getValidationAdministratif());
            preFind.setValidationJuridique(newPc.getValidationJuridique());
            preFind.setValidationPedagogique(newPc.getValidationPedagogique());
        } else {
            // Ajout à la base de données
            // C'est une maquette: aucune vérification de l'existance de l'étudiant
            Etudiant e = new Etudiant();
            e.setNomEtudiant(newPc.getEtudiantPC().getNomEtudiant());
            e.setPrenomEtudiant(newPc.getEtudiantPC().getPrenomEtudiant());
            etudiantFacade.create(e);
            
            Preconvention pc = new fr.miage.m2.convmakerDashboard.entities.Preconvention();
            pc.setNoPC(newPc.getNoPC());
            pc.setEtudiantPC(e);
            pc.setDateDebutPC(newPc.getDateDebutPC().getTime());
            pc.setDateFinPC(newPc.getDateFinPC().getTime());
            pc.setGratificationPC(newPc.getGratificationPC());
            pc.setResumePC(newPc.getResumePC());
            pc.setEtatPC(newPc.getEtatPC().toString());
            pc.setNomEntreprisePC(newPc.getNomEntreprisePC());
            pc.setSirenEntreprisePC(newPc.getSirenEntreprisePC());
            pc.setCodeAssurancePC(newPc.getCodeAssurancePC());
            pc.setNomAssurancePC(newPc.getNomAssurancePC());
            pc.setNiveau(newPc.getNiveau());
            pc.setIntituleFormation(newPc.getIntituleFormation());
            pc.setTuteurEnseignant(newPc.getTuteurEnseignant());
            if (newPc.getRaisonsRefus().size() > 0) {
                pc.setRaisonsRefus(String.join(";", newPc.getRaisonsRefus()));
            }
            pc.setValidationAdministratif(newPc.getValidationAdministratif());
            pc.setValidationJuridique(newPc.getValidationJuridique());
            pc.setValidationPedagogique(newPc.getValidationPedagogique());
            preconventionFacade.create(pc);
        }
        
        // Attention: le service Dashboard ne reçoit que les préconventions CONFORME
    }
}