package fr.miage.m2.convmakerDashboard.messages;

import java.io.Serializable;

/**
 *
 */
public class Enseignant implements Serializable {
    
    private int noEns;
    
    private String nomEns;
    
    private String prenomEns;

    public Enseignant(int noEns, String nomEns, String prenomEns) {
        this.noEns = noEns;
        this.nomEns = nomEns;
        this.prenomEns = prenomEns;
    }

    public Enseignant() {
    }

    public int getNoEns() {
        return noEns;
    }

    public String getNomEns() {
        return nomEns;
    }
    
    public String getPrenomEns() {
        return prenomEns;
    }

    public void setNoEns(int noEns) {
        this.noEns = noEns;
    }

    public void setNomEns(String nomEns) {
        this.nomEns = nomEns;
    }

    public void setPrenomEns(String prenomEns) {
        this.prenomEns = prenomEns;
    }

    @Override
    public String toString() {
        return "Enseignant{" + "noEns=" + noEns + ", nomEns=" + nomEns + ", prenomEns=" + prenomEns + '}';
    }
    
}
