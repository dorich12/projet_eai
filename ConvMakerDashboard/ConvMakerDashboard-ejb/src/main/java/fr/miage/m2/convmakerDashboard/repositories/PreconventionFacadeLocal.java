/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.m2.convmakerDashboard.repositories;

import fr.miage.m2.convmakerDashboard.entities.Preconvention;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author David
 */
@Local
public interface PreconventionFacadeLocal {

    void create(Preconvention preconvention);

    void edit(Preconvention preconvention);

    void remove(Preconvention preconvention);

    Preconvention find(Object id);

    List<Preconvention> findAll();

    List<Preconvention> findRange(int[] range);

    int count();
    
    public Preconvention findByNoPc(int noPc);
    
}
