package fr.miage.m2.convmakerDashboard.webserver;

import fr.miage.m2.convmakershared.Etudiant;
import java.util.ArrayList;
import javax.ejb.Stateless;

/**
 *
 */
@Stateless
public class EtudiantsSingleton {
    
    private final ArrayList<Etudiant> etudiants;

    public EtudiantsSingleton() {
        // Données de test
        etudiants = new ArrayList<>();
        Etudiant dorian = new Etudiant(1,"RICHARD","Dorian");
        Etudiant david = new Etudiant(2,"BERNARD","David");
        Etudiant gaetan = new Etudiant(3,"VENUAT","Gaétan");
        
        etudiants.add(dorian);
        etudiants.add(david);
        etudiants.add(gaetan);
    }
    
    public ArrayList<Etudiant> getEtudiants() {
        return etudiants;
    }
    
    public Etudiant creeEtudiant(int numeroE, String nom, String prenom) {
        Etudiant etu = new Etudiant(numeroE, nom, prenom);
        etudiants.add(etu);
        
        return etu;
    }
}