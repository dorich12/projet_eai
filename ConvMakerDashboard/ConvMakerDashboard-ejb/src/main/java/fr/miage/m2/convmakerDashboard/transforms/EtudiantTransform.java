/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.m2.convmakerDashboard.transforms;

import fr.miage.m2.convmakershared.Etudiant;

/**
 *
 */
public class EtudiantTransform {
    
    /**
     * Transforme une entité Etudiant en objet Etudiant partagé
     * @param etuEnt
     * @return etudiant
     */
    public static Etudiant EntityToResource(fr.miage.m2.convmakerDashboard.entities.Etudiant etuEnt) {
        Etudiant e = new Etudiant();
        e.setNoEtudiant(etuEnt.getNoEtudiant());
        e.setNomEtudiant(etuEnt.getNomEtudiant());
        e.setPrenomEtudiant(etuEnt.getPrenomEtudiant());
        
        return e;
    }
    
}
