/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.m2.convmakerDashboard.transforms;

import fr.miage.m2.convmakershared.EtatPreconvention;
import fr.miage.m2.convmakershared.Preconvention;
import java.util.Calendar;

/**
 *
 */
public class PreconventionTransform {
    
    /**
     * Transforme une entité Preconvention en object Preconvention partagé
     * @param pcEnt
     * @return preconvention
     */
    public static Preconvention entityToResource(fr.miage.m2.convmakerDashboard.entities.Preconvention pcEnt) {
        Preconvention pc = new Preconvention();
        pc.setNoPC(pcEnt.getNoPC());
        pc.setEtudiantPC(EtudiantTransform.EntityToResource(pcEnt.getEtudiantPC()));
        
        // Gestion des dates
        if (pcEnt.getDateDebutPC() != null) {
            Calendar calDebut = Calendar.getInstance();
            calDebut.setTime(pcEnt.getDateDebutPC());
            pc.setDateDebutPC(calDebut);
        }
        if (pcEnt.getDateFinPC() != null) {
            Calendar calFin = Calendar.getInstance();
            calFin.setTime(pcEnt.getDateFinPC());
            pc.setDateFinPC(calFin);
        }
        
        pc.setGratificationPC(pcEnt.getGratificationPC());
        pc.setResumePC(pcEnt.getResumePC());
        pc.setEtatPC(EtatPreconvention.valueOf(pcEnt.getEtatPC()));
        pc.setNomEntreprisePC(pcEnt.getNomEntreprisePC());
        pc.setSirenEntreprisePC(pcEnt.getSirenEntreprisePC());
        pc.setCodeAssurancePC(pcEnt.getCodeAssurancePC());
        pc.setNomAssurancePC(pcEnt.getNomAssurancePC());
        pc.setNiveau(pcEnt.getNiveau());
        pc.setIntituleFormation(pcEnt.getIntituleFormation());
        pc.setTuteurEnseignant(pcEnt.getTuteurEnseignant());
        pc.setValidationAdministratif(pcEnt.getValidationAdministratif());
        pc.setValidationJuridique(pcEnt.getValidationJuridique());
        pc.setValidationPedagogique(pcEnt.getValidationPedagogique());
        // Note: pas de raisonRefus car Dashboard enregistre que les préconventions CONFORME
        
        return pc;
    }
    
}
