/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.m2.convmakerDashboard.repositories;

import fr.miage.m2.convmakerDashboard.entities.Preconvention;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author David
 */
@Stateless
public class PreconventionFacade extends AbstractFacade<Preconvention> implements PreconventionFacadeLocal {

    @PersistenceContext(unitName = "fr.miage.m2_ConvMakerDashboard-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PreconventionFacade() {
        super(Preconvention.class);
    }
    
    @Override
    public Preconvention findByNoPc(int noPc) {
        Preconvention pc = null;
        try {
            Query q = em.createQuery(
               "SELECT p FROM Preconvention p WHERE p.noPC = :id");
            q.setParameter("id", noPc);
            pc = (Preconvention) q.getSingleResult();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
        
        return pc;
    }
    
}
