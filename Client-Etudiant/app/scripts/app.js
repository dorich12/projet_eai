'use strict';

/**
 * @ngdoc overview
 * @name clientEtudiantApp
 * @description
 * # clientEtudiantApp
 *
 * Main module of the application.
 */
angular
  .module('clientEtudiantApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'ConventionCtrl',
        controllerAs: 'main'
      })
      .when('/suivi', {
        templateUrl: 'views/suivi.html',
        controller: 'SuiviCtrl',
        controllerAs: 'suivi'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
