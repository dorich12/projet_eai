'use strict';

angular.module('clientEtudiantApp')
  .controller('SuiviCtrl', ['$scope', 'gestionConventionService', function($scope, gestionConventionService) {

    $scope.idConvention = null;

  	// Récupération de la convention
    $scope.demandeSuivi = function demandeSuivi(){
        if ($scope.idConvention) {
            gestionConventionService.getPreConvention($scope.idConvention)
                .then(
                    function(d) {
                        $scope.conventionTrouvee = d;
                    },
                function(errResponse){
                    console.error('Error while creating account', errResponse);
                }
            );
        }
    }
    
}]);