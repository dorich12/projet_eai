'use strict';

angular.module('clientEtudiantApp')
  .controller('ConventionCtrl', ['$scope', 'gestionConventionService', function($scope, gestionConventionService) {

  	// Création de l'object
    $scope.convention = {
        nomE : 'DURAND',
        prenomE : 'Bruno',
        numeroE : '21445276',
        niveau : 'M2',
        intitule : 'MIAGE',
        assurance : 'MACIF',
        numContrat : '39427',
        entreprise : 'SOGETI',
        siren : '479942583',
        dateD : '10/09/2018',
        dateF : '10/09/2019',
        gratification : '576',
        description : 'Description du stage'
    }

  	//Création d'une nouvelle convention dans le système
    $scope.createConvention = function createConvention(convention){
        gestionConventionService.createConvention(convention)
            .then(
                function(d) {
                    $scope.conventionValide = d;
                },
            function(errResponse){
                console.error('Error while creating convention', errResponse);
            }
        );
    }
    
}]);
