'use strict';

angular.module('clientEtudiantApp').factory('gestionConventionService', ['$http', '$q', function($http, $q) {

    // URL de l'API REST
    var REST_SERVICE_URI = "http://localhost:8080/ConvMaker-web/webresources/PreConvention";

    // Déclaration des fonctions
    var factory = {
      createConvention: createConvention,
      getPreConvention: getPreConvention
    };

    return factory;

    // Ajout d'une nouvelle convetion dans le système
    function createConvention(convention) {
        var deferred = $q.defer();

        $http.post(REST_SERVICE_URI, JSON.stringify(convention))
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating convention');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    // Récupère une convention
    function getPreConvention(conventionID) {
        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + "/" + conventionID)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while get convention');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
}]);
