'use strict';

angular.module('clientJuridiqueApp')
  .controller('ConventionCtrl', ['$scope', 'gestionConventionService', function($scope, gestionConventionService) {

  	// Récupération des préConventions à traiter
    function getPreConventions(){
        gestionConventionService.getPreConventions()
            .then(
                function(d) {
                    $scope.conventions = d;
                },
            function(errResponse){
                console.error('Error while getting conventions', errResponse);
            }
        );
    }
    getPreConventions();
    
}]);
