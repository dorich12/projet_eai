'use strict';

angular.module('clientJuridiqueApp')
  .controller('SuiviCtrl', ['$scope', 'gestionConventionService', 'verificationINSEEService', 'verificationAssuranceService', '$routeParams', function($scope, gestionConventionService, verificationINSEEService, verificationAssuranceService, $routeParams) {

    $scope.idConvention = $routeParams.id;
    $scope.conventionTrouvee = null;

  	// Récupération de la convention
    function getPreConvention() {
        if ($scope.idConvention) {
            gestionConventionService.getPreConvention($scope.idConvention)
                .then(
                    function(d) {
                        $scope.conventionTrouvee = d;
                    },
                function(errResponse){
                    console.error('Error while creating account', errResponse);
                }
            );
        }
    }
    getPreConvention();

    $scope.modifierValidation = function modifierValidation(num) {
        if ($scope.conventionTrouvee) {
            $scope.conventionTrouvee.validationJuridique = num;

            if (num == 2) {
                // On demande le motif du refus
                var reason = prompt("Veuillez indiquer le motif du refus :", "");
                $scope.conventionTrouvee.raisonsRefus.push(reason);
            }
            
            gestionConventionService.postPreConvention($scope.conventionTrouvee)
                .then(
                    function(d) {
                        // To do
                    },
                function(errResponse){
                    console.error('Error while creating account', errResponse);
                }
            );
        }
    }

    $scope.verifierINSEE = function verifierINSEE() {
        if ($scope.conventionTrouvee) {
            verificationINSEEService.getSirenVerification($scope.conventionTrouvee.sirenEntreprisePC)
                .then(
                    function(d) {
                        if (d.status == true) {
                            alert("Entreprise trouvée !\nRaison sociale : " + d.raisonSociale + "\nDate création : " + d.dateCreation + "\nActivités : " + d.activites);
                        } else {
                            alert('L\'entreprise n\'est pas trouvée !');
                        }

                        $scope.checked3 = d.status;
                    },
                function(errResponse){
                    console.error('Error while creating account', errResponse);
                }
            );
        }
    }

    $scope.verifierAssurance = function verifierAssurance() {
        if ($scope.conventionTrouvee) {
            verificationAssuranceService.getVerification($scope.conventionTrouvee)
                .then(
                    function(d) {
                        if (d == true) {
                            alert("Assurance valide !");
                        } else {
                            alert('Assurance non valide !');
                        }

                        $scope.checked4 = d;
                    },
                function(errResponse){
                    console.error('Error while creating account', errResponse);
                }
            );
        }
    }
    
}]);

angular.module('clientJuridiqueApp')
    .filter('split', function() {
        return function(input, splitChar, splitIndex) {
            // do some bounds checking here to ensure it has that index
            return input.split(splitChar)[splitIndex];
        }
    });