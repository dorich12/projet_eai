'use strict';

angular.module('clientJuridiqueApp').factory('verificationINSEEService', ['$http', '$q', function($http, $q) {

    // URL de l'API REST
    var REST_SERVICE_URI = "http://localhost:8080/ConvMakerJuridique-web/webresources/siren";

    // Déclaration des fonctions
    var factory = {
      getSirenVerification: getSirenVerification
    };

    return factory;

    // Récupère la préconvention par son ID
    function getSirenVerification(siren) {
        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + '/' + siren)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while get INSEE verification');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

}]);
