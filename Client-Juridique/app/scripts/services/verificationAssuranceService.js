'use strict';

angular.module('clientJuridiqueApp').factory('verificationAssuranceService', ['$http', '$q', function($http, $q) {

    // URL de l'API REST
    var REST_SERVICE_URI = "https://hdzg7dtrz3.execute-api.us-east-1.amazonaws.com/dev/assurance/check";

    // Déclaration des fonctions
    var factory = {
      getVerification: getVerification
    };

    return factory;

    // Exemple : { nom_assurance: « test», «  nom_personne»: « test », « numero_contrat »:1234, « "date_debut_contrat »: 123456, « date_fin_contrat »: 456789}

    // Récupère la préconvention par son ID
    function getVerification(conv) {
        var deferred = $q.defer();

        // Création de l'object de la demande
        var obj = {
            nom_assurance : conv.nomAssurancePC,
            nom_personne : conv.etudiantPC.nomEtudiant,
            numero_contrat : conv.codeAssurancePC,
            date_debut_contrat : conv.dateDebutPC,
            date_fin_contrat : conv.dateFinPC
        }

        $http.post(REST_SERVICE_URI, obj)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while get assurance verification');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

}]);
