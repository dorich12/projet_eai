'use strict';

angular.module('clientDashboardApp')
  .controller('SuiviCtrl', ['$scope', 'gestionConventionService', '$routeParams', function($scope, gestionConventionService, $routeParams) {

    $scope.idConvention = $routeParams.id;
    $scope.conventionTrouvee = null;

  	// Récupération de la convention
    function getPreConvention(){
        if ($scope.idConvention) {
            gestionConventionService.getPreConvention($scope.idConvention)
                .then(
                    function(d) {
                        $scope.conventionTrouvee = d;
                    },
                function(errResponse){
                    console.error('Error while creating account', errResponse);
                }
            );
        }
    }
    getPreConvention();
    
}]);

angular.module('clientDashboardApp')
    .filter('split', function() {
        return function(input, splitChar, splitIndex) {
            // do some bounds checking here to ensure it has that index
            return input.split(splitChar)[splitIndex];
        }
    });