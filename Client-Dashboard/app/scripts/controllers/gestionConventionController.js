'use strict';

angular.module('clientDashboardApp')
  .controller('ConventionCtrl', ['$scope', 'gestionConventionService', function($scope, gestionConventionService) {

  	// Récupération des préConventions à traiter
    function getPreConventions(){
        gestionConventionService.getPreConventions()
            .then(
                function(d) {
                    $scope.conventions = d;

                    // Calcul des statistiques
                    $scope.nbPC = 0;
                    $scope.totalGratification = 0;
                    for (var i=0; i<$scope.conventions.length; i++) {
                        $scope.nbPC++;
                        if ($scope.conventions[i].gratificationPC) {
                            $scope.totalGratification += $scope.conventions[i].gratificationPC;
                        }
                    }
                    if ($scope.nbPC > 0) {
                        $scope.moyenneGratification = $scope.totalGratification / $scope.nbPC;
                    }
                },
            function(errResponse){
                console.error('Error while getting conventions', errResponse);
            }
        );
    }
    getPreConventions();
    
}]);
