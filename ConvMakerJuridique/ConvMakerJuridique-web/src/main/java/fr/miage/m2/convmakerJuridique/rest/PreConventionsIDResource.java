/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.m2.convmakerJuridique.rest;

import fr.miage.m2.convmakershared.Preconvention;
import fr.miage.m2.convmakerJuridique.webserver.PreconventionsSingleton;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.enterprise.context.RequestScoped;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author David
 */
@Path("PreConvention/{id}")
@RequestScoped
public class PreConventionsIDResource {
    
    PreconventionsSingleton preconventionsSingleton = lookupPreconventionsSingletonBean();

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PreConventionsIDResource
     */
    public PreConventionsIDResource() {
    }

    /**
     * GET Récupère la préconvention d'id passé en paramètre
     * @param id
     * @return preconvention
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Preconvention getOne(@PathParam("id") int id) {
        return preconventionsSingleton.getPreconvention(id);
    }
    
    /**
     * POST Met à jour la préconvention
     * @param preconvention
     * @return preconvention modifiée
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Preconvention editOne(Preconvention preconvention) {
        return preconventionsSingleton.editPreconvention(preconvention);
    }
    
    /**
     * Récupère l'instance du bean PreconventionSingleton
     * @return PreconventionsSingleton bean
     */
    private PreconventionsSingleton lookupPreconventionsSingletonBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (PreconventionsSingleton) c.lookup("java:global/ConvMakerJuridique-ear/ConvMakerJuridique-ejb-1.0-SNAPSHOT/PreconventionsSingleton!fr.miage.m2.convmakerJuridique.webserver.PreconventionsSingleton");
        } catch (NamingException ne) {
            throw new RuntimeException(ne);
        }
    }
}
