/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.m2.convmakerJuridique.rest;

import com.google.gson.Gson;
import fr.miage.m2.convmakerJuridique.metiersiren.Records;
import fr.miage.m2.convmakerJuridique.metiersiren.SirenPOJO;
import fr.miage.m2.convmakerJuridique.ressources.SirenRessource;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.PathParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 */
@Path("siren/{id}")
@RequestScoped
public class siren {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of siren
     */
    public siren() {
    }

    /**
     * Get SIREN verification from INSEE API
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public SirenRessource getSirenVerification(@PathParam("id") String siren) {
        // TOKEN BEARER a récuperer sur INSEE
        String token = "Bearer 87faafeb-b34f-39d4-8cc0-cb9e7a15a8d9";
        // URI Service INSEE
        String uri = "http://data.opendatasoft.com/api/records/1.0/search/?dataset=sirene%40public";

        // a ajuster selon requete voir mode emploi INSEE
        String query = "&lang=fr";
        
        // I/O JSON
        Gson gson = new Gson();

        Client client = ClientBuilder.newClient();
        WebTarget wt = client.target(uri + "&q=" + siren + query);

        //WebResource webResource = client.resource(uri + siren + query);
        //System.out.println("uri appel: " + uri + "&q=" + siren + query);

        Invocation.Builder invocationBuilder = wt.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        String reponse = response.readEntity(String.class);

        // Convertisseur JSON
        SirenPOJO model = gson.fromJson(reponse, SirenPOJO.class);

        // System.out.println("Résultat: " + response.getStatus());
        Records[] rec = model.getRecords();
        if (rec.length != 0) {
            for(int i = 0; i < rec.length ; i++) {
                SirenRessource sR = new SirenRessource(true, model.getRecords()[i].getFields().getL1_normalisee(), model.getRecords()[i].getFields().getDcren(), model.getRecords()[i].getFields().getActivite());
                return sR;
            }
        }
        
        SirenRessource sR = new SirenRessource(false, "", "", "");
        return sR;
    }

    /**
     * PUT method for updating or creating an instance of siren
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
