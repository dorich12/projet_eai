/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.m2.convmakerJuridique.ressources;

import java.io.Serializable;

/**
 *
 */
public class SirenRessource implements Serializable {
    
    private boolean status;
    
    private String raisonSociale;
    
    private String dateCreation;
    
    private String activites;

    public SirenRessource(boolean status, String raisonSociale, String dateCreation, String activites) {
        this.status = status;
        this.raisonSociale = raisonSociale;
        this.dateCreation = dateCreation;
        this.activites = activites;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getActivites() {
        return activites;
    }

    public void setActivites(String activites) {
        this.activites = activites;
    }

    @Override
    public String toString() {
        return "SirenRessource{" + "status=" + status + ", raisonSociale=" + raisonSociale + ", dateCreation=" + dateCreation + ", activites=" + activites + '}';
    }
}
