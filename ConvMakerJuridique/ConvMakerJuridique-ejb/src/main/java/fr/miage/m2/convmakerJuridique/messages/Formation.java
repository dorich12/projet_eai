package fr.miage.m2.convmakerJuridique.messages;

import java.io.Serializable;

/**
 *
 */
public class Formation implements Serializable {
    
    private int noFormation;
    
    private String codeFormation;
    
    private String intituleFormation;
    
    private String niveauFormation;
    
    private String departFormation;

    public Formation(int noFormation, String codeFormation, String intituleFormation, String niveauFormation, String departFormation) {
        this.noFormation = noFormation;
        this.codeFormation = codeFormation;
        this.intituleFormation = intituleFormation;
        this.niveauFormation = niveauFormation;
        this.departFormation = departFormation;
    }

    public Formation(){
        
    }

    public int getNoFormation() {
        return noFormation;
    }

    public void setNoFormation(int noFormation) {
        this.noFormation = noFormation;
    }

    public String getCodeFormation() {
        return codeFormation;
    }

    public void setCodeFormation(String codeFormation) {
        this.codeFormation = codeFormation;
    }

    public String getIntituleFormation() {
        return intituleFormation;
    }

    public void setIntituleFormation(String intituleFormation) {
        this.intituleFormation = intituleFormation;
    }

    public String getNiveauFormation() {
        return niveauFormation;
    }

    public void setNiveauFormation(String niveauFormation) {
        this.niveauFormation = niveauFormation;
    }

    public String getDepartFormation() {
        return departFormation;
    }

    public void setDepartFormation(String departFormation) {
        this.departFormation = departFormation;
    }

    @Override
    public String toString() {
        return "Formation{" + "noFormation=" + noFormation + ", codeFormation=" + codeFormation + ", intituleFormation=" + intituleFormation + ", niveauFormation=" + niveauFormation + ", departFormation=" + departFormation + '}';
    }
    
}