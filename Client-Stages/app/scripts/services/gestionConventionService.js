'use strict';

angular.module('clientStagesApp').factory('gestionConventionService', ['$http', '$q', function($http, $q) {

    // URL de l'API REST
    var REST_SERVICE_URI = "http://localhost:8080/ConvMakerStages-web/webresources/PreConvention";

    // Déclaration des fonctions
    var factory = {
      getPreConventions: getPreConventions,
      getPreConvention: getPreConvention
    };

    return factory;

    // Récupère les préConventions à traiter
    function getPreConventions() {
        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while get conventions');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    // Récupère la préconvention par son ID
    function getPreConvention(conventionID) {
        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + '/' + conventionID)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while get convention');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

}]);
