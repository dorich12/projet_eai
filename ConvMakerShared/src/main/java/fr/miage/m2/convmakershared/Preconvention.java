/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.m2.convmakershared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 */
public class Preconvention implements Serializable {
    
    private int noPC;
    
    private Etudiant etudiantPC;

    private Calendar dateDebutPC;
    
    private Calendar dateFinPC;
    
    private float gratificationPC;
    
    private String resumePC;
    
    private EtatPreconvention etatPC;
    
    private String nomEntreprisePC;
    
    private int sirenEntreprisePC;
    
    private int codeAssurancePC;
    
    private String nomAssurancePC;
    
    private String niveau;
    
    private String intituleFormation;
    
    private String tuteurEnseignant;
    
    private List<String> raisonsRefus;
    
    private int validationJuridique; // 0: init; 1: ok; 2: refus
    
    private int validationPedagogique; // 0: init; 1: ok; 2: refus
    
    private int validationAdministratif; // 0: init; 1: ok; 2: refus

    public Preconvention(int noPC, Etudiant etudiantPC, Calendar dateDebutPC, Calendar dateFinPC, float gratificationPC, String resumePC, EtatPreconvention etatPC, String nomEntreprisePC, int sirenEntreprisePC, int codeAssurancePC, String nomAssurancePC, String niveau, String intituleFormation) {
        this.noPC = noPC;
        this.etudiantPC = etudiantPC;
        this.dateDebutPC = dateDebutPC;
        this.dateFinPC = dateFinPC;
        this.gratificationPC = gratificationPC;
        this.resumePC = resumePC;
        this.etatPC = etatPC;
        this.nomEntreprisePC = nomEntreprisePC;
        this.sirenEntreprisePC = sirenEntreprisePC;
        this.codeAssurancePC = codeAssurancePC;
        this.nomAssurancePC = nomAssurancePC;
        this.niveau = niveau;
        this.intituleFormation = intituleFormation;
        this.tuteurEnseignant = "";
        this.raisonsRefus = new ArrayList<>();
        this.validationJuridique = 0;
        this.validationPedagogique = 0;
        this.validationAdministratif = 0;
    }
    
    public Preconvention(){
        
    }

    public int getNoPC() {
        return noPC;
    }

    public void setNoPC(int noPC) {
        this.noPC = noPC;
    }

    public Etudiant getEtudiantPC() {
        return etudiantPC;
    }

    public void setEtudiantPC(Etudiant etudiantPC) {
        this.etudiantPC = etudiantPC;
    }

    public Calendar getDateDebutPC() {
        return dateDebutPC;
    }

    public void setDateDebutPC(Calendar dateDebutPC) {
        this.dateDebutPC = dateDebutPC;
    }

    public Calendar getDateFinPC() {
        return dateFinPC;
    }

    public void setDateFinPC(Calendar dateFinPC) {
        this.dateFinPC = dateFinPC;
    }

    public float getGratificationPC() {
        return gratificationPC;
    }

    public void setGratificationPC(float gratificationPC) {
        this.gratificationPC = gratificationPC;
    }

    public String getResumePC() {
        return resumePC;
    }

    public void setResumePC(String resumePC) {
        this.resumePC = resumePC;
    }

    public EtatPreconvention getEtatPC() {
        return etatPC;
    }

    public void setEtatPC(EtatPreconvention etatPC) {
        this.etatPC = etatPC;
    }

    public String getNomEntreprisePC() {
        return nomEntreprisePC;
    }

    public void setNomEntreprisePC(String nomEntreprisePC) {
        this.nomEntreprisePC = nomEntreprisePC;
    }

    public int getSirenEntreprisePC() {
        return sirenEntreprisePC;
    }

    public void setSirenEntreprisePC(int sirenEntreprisePC) {
        this.sirenEntreprisePC = sirenEntreprisePC;
    }

    public int getCodeAssurancePC() {
        return codeAssurancePC;
    }

    public void setCodeAssurancePC(int codeAssurancePC) {
        this.codeAssurancePC = codeAssurancePC;
    }

    public String getNomAssurancePC() {
        return nomAssurancePC;
    }

    public void setNomAssurancePC(String nomAssurancePC) {
        this.nomAssurancePC = nomAssurancePC;
    }

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public String getIntituleFormation() {
        return intituleFormation;
    }

    public void setIntituleFormation(String intituleFormation) {
        this.intituleFormation = intituleFormation;
    }

    public String getTuteurEnseignant() {
        return tuteurEnseignant;
    }

    public void setTuteurEnseignant(String tuteurEnseignant) {
        this.tuteurEnseignant = tuteurEnseignant;
    }

    public List<String> getRaisonsRefus() {
        return raisonsRefus;
    }

    public void setRaisonsRefus(List<String> raisonsRefus) {
        this.raisonsRefus = raisonsRefus;
    }

    public int getValidationJuridique() {
        return validationJuridique;
    }

    public void setValidationJuridique(int validationJuridique) {
        this.validationJuridique = validationJuridique;
    }

    public int getValidationPedagogique() {
        return validationPedagogique;
    }

    public void setValidationPedagogique(int validationPedagogique) {
        this.validationPedagogique = validationPedagogique;
    }

    public int getValidationAdministratif() {
        return validationAdministratif;
    }

    public void setValidationAdministratif(int validationAdministratif) {
        this.validationAdministratif = validationAdministratif;
    }

    @Override
    public String toString() {
        return "Preconvention{" + "noPC=" + noPC + ", etudiantPC=" + etudiantPC + ", dateDebutPC=" + dateDebutPC + ", dateFinPC=" + dateFinPC + ", gratificationPC=" + gratificationPC + ", resumePC=" + resumePC + ", etatPC=" + etatPC + ", nomEntreprisePC=" + nomEntreprisePC + ", sirenEntreprisePC=" + sirenEntreprisePC + ", codeAssurancePC=" + codeAssurancePC + ", nomAssurancePC=" + nomAssurancePC + ", niveau=" + niveau + ", intituleFormation=" + intituleFormation + ", tuteurEnseignant=" + tuteurEnseignant + ", raisonsRefus=" + raisonsRefus + ", validationJuridique=" + validationJuridique + ", validationPedagogique=" + validationPedagogique + ", validationAdministratif=" + validationAdministratif + '}';
    }
}
