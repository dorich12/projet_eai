/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.m2.convmakershared;

import java.io.Serializable;

/**
 *
 */
public class Etudiant implements Serializable {
    
    private int noEtudiant;
    
    private String nomEtudiant;
    
    private String prenomEtudiant;

    public Etudiant(int noEtudiant, String nomEtudiant, String prenomEtudiant) {
        this.noEtudiant = noEtudiant;
        this.nomEtudiant = nomEtudiant;
        this.prenomEtudiant = prenomEtudiant;
    }

    public Etudiant(){
        
    }

    public int getNoEtudiant() {
        return noEtudiant;
    }

    public void setNoEtudiant(int noEtudiant) {
        this.noEtudiant = noEtudiant;
    }

    public String getNomEtudiant() {
        return nomEtudiant;
    }

    public void setNomEtudiant(String nomEtudiant) {
        this.nomEtudiant = nomEtudiant;
    }

    public String getPrenomEtudiant() {
        return prenomEtudiant;
    }

    public void setPrenomEtudiant(String prenomEtudiant) {
        this.prenomEtudiant = prenomEtudiant;
    }

    @Override
    public String toString() {
        return "Etudiant{" + "noEtudiant=" + noEtudiant + ", nomEtudiant=" + nomEtudiant + ", prenomEtudiant=" + prenomEtudiant + '}';
    }

}
