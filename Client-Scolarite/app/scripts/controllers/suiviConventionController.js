'use strict';

angular.module('clientScolariteApp')
  .controller('SuiviCtrl', ['$scope', 'gestionConventionService', '$routeParams', function($scope, gestionConventionService, $routeParams) {

    $scope.idConvention = $routeParams.id;
    $scope.conventionTrouvee = null;

  	// Récupération de la convention
    function getPreConvention(){
        if ($scope.idConvention) {
            gestionConventionService.getPreConvention($scope.idConvention)
                .then(
                    function(d) {
                        $scope.conventionTrouvee = d;
                    },
                function(errResponse){
                    console.error('Error while creating account', errResponse);
                }
            );
        }
    }
    getPreConvention();

    $scope.modifierValidation = function modifierValidation(num){
        if ($scope.conventionTrouvee) {
            $scope.conventionTrouvee.validationAdministratif = num;

            if (num == 2) {
                // On demande le motif du refus
                var reason = prompt("Veuillez indiquer le motif du refus :", "");
                $scope.conventionTrouvee.raisonsRefus.push(reason);
            }

            gestionConventionService.postPreConvention($scope.conventionTrouvee)
                .then(
                    function(d) {
                        // To do
                    },
                function(errResponse){
                    console.error('Error while creating account', errResponse);
                }
            );
        }
    }
    
}]);

angular.module('clientScolariteApp')
    .filter('split', function() {
        return function(input, splitChar, splitIndex) {
            // do some bounds checking here to ensure it has that index
            return input.split(splitChar)[splitIndex];
        }
    });