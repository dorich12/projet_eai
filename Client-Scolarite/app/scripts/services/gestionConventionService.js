'use strict';

angular.module('clientScolariteApp').factory('gestionConventionService', ['$http', '$q', function($http, $q) {

    // URL de l'API REST
    var REST_SERVICE_URI = "http://localhost:8080/ConvMakerScolarite-web/webresources/PreConvention";

    // Déclaration des fonctions
    var factory = {
      getPreConventions: getPreConventions,
      getPreConvention: getPreConvention,
      postPreConvention: postPreConvention
    };

    return factory;

    // Récupère les préConventions à traiter
    function getPreConventions() {
        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while get conventions');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    // Récupère la préconvention par son ID
    function getPreConvention(conventionID) {
        var deferred = $q.defer();

        $http.get(REST_SERVICE_URI + '/' + conventionID)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while get convention');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    // Modifie la préconvention
    function postPreConvention(convention) {
        var deferred = $q.defer();

        $http.post(REST_SERVICE_URI + '/' + convention.noPC, convention)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while post convention');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

}]);
