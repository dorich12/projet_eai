/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.m2.convmaker.rest;

import fr.miage.m2.convmakershared.Preconvention;
import fr.miage.m2.convmaker.webserver.PreconventionsSingleton;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.enterprise.context.RequestScoped;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author David
 */
@Path("PreConvention/{id}")
@RequestScoped
public class PreConventionsIDResource {
    
    PreconventionsSingleton preconventionsSingleton = lookupPreconventionsSingletonBean();

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PreConventionsIDResource
     */
    public PreConventionsIDResource() {
    }

    /**
     * GET Récupère la préconvention d'id passé en paramètre
     * @param id
     * @return 
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Preconvention getOne(@PathParam("id") int id) {
        return preconventionsSingleton.getPreconvention(id);
    }
    
    /**
     * Récupère l'instance du bean PreconventionSingleton
     * @return PreconventionsSingleton bean
     */
    private PreconventionsSingleton lookupPreconventionsSingletonBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (PreconventionsSingleton) c.lookup("java:global/ConvMaker-ear/ConvMaker-ejb-1.0/PreconventionsSingleton!fr.miage.m2.convmaker.webserver.PreconventionsSingleton");
        } catch (NamingException ne) {
            throw new RuntimeException(ne);
        }
    }
}
