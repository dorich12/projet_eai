package fr.miage.m2.convmaker.rest;

import fr.miage.m2.convmakershared.Preconvention;
import fr.miage.m2.convmaker.ressources.PreConventionRessource;
import fr.miage.m2.convmaker.webserver.PreconventionsSingleton;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * REST Web Service
 *
 */
@Path("PreConvention")
public class PreconventionsRessource {
    
    PreconventionsSingleton preconventionsSingleton = lookupPreconventionsSingletonBean();

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PreconventionsRessource
     */
    public PreconventionsRessource() {
    }
    
    /**
     * POST créer une préconvention
     * @param content representation for the resource
     * @return preconvention crée
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Preconvention creePreConvention(PreConventionRessource content) {
        try {
            // Gestion des dates
            DateFormat formatter = new SimpleDateFormat("dd/MM/YYYY");
            Calendar dateD = Calendar.getInstance();
            dateD.setTime(formatter.parse(content.getDateD()));
            Calendar dateF = Calendar.getInstance();
            dateD.setTime(formatter.parse(content.getDateF()));
            
            return preconventionsSingleton.creerPreconvention(content.getNomE(), content.getPrenomE(), content.getNumeroE(),
                    dateD, dateF, content.getGratification(), content.getDescription(), content.getEntreprise(),
                    content.getSiren(), content.getNumContrat(), content.getAssurance(), content.getNiveau(), content.getIntitule());
        } catch (ParseException ex) {
            Logger.getLogger(PreconventionsRessource.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    /**
     * Récupère l'instance du bean PreconventionSingleton
     * @return PreconventionsSingleton bean
     */
    private PreconventionsSingleton lookupPreconventionsSingletonBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (PreconventionsSingleton) c.lookup("java:global/ConvMaker-ear/ConvMaker-ejb-1.0/PreconventionsSingleton!fr.miage.m2.convmaker.webserver.PreconventionsSingleton");
        } catch (NamingException ne) {
            throw new RuntimeException(ne);
        }
    }
    
}
