package fr.miage.m2.convmaker.webserver;

import fr.miage.m2.convmakershared.Preconvention;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 *
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "clientId", propertyValue = "preconv")
    ,
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "preconv")
    ,
        @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "preconv")
    ,
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic")
})
public class MiseAJourPreconvention implements MessageListener {
    
    @EJB
    PreconventionsSingleton preconventions;
    
    public MiseAJourPreconvention() {
    }
    
    @Override
    public void onMessage(Message message) {
        if (message instanceof ObjectMessage) {
             try {
                 ObjectMessage om = (ObjectMessage) message;
                 Object obj = om.getObject();
                 if (obj instanceof Preconvention) {
                     Preconvention pc = (Preconvention) obj;
                     // System.out.println("Préconvention " + pc.getNoPC() + " reçue sur étudiant");
                     preconventions.miseAJourPreconvention(pc);
                 }
             } catch (JMSException ex) {
                 Logger.getLogger(MiseAJourPreconvention.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
    }
    
}
