package fr.miage.m2.convmaker.webserver;

import java.util.ArrayList;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import fr.miage.m2.convmakershared.Preconvention;
import fr.miage.m2.convmakershared.EtatPreconvention;
import fr.miage.m2.convmakershared.Etudiant;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Topic;

/**
 *
 */
@Singleton
@LocalBean
public class PreconventionsSingleton {
    
    @Resource(lookup = "preconv")
    private Topic topic;
    
    @Inject
    private JMSContext context;
    
    @EJB
    EtudiantsSingleton etudiants;
    
    private final ArrayList<Preconvention> preconventions;

    public PreconventionsSingleton() {
        preconventions = new ArrayList<>();
    }

    /**
     * Récupère l'ensemble des Préconvetion de ce service
     * @return 
     */
    public ArrayList<Preconvention> getPreconventions() {
        return preconventions;
    }
    
    /**
     * Récupère la préconvention d'id passé en paramètre
     * @param id
     * @return preconvention ou null
     */
    public Preconvention getPreconvention(int id) {
        return this.rechercher(id);
    }

    /**
     * Créer une Preconvention et l'ajoute à la liste en mémoire
     * @param nomE
     * @param prenomE
     * @param numeroE
     * @param dateDebutPC
     * @param dateFinPC
     * @param gratificationPC
     * @param resumePC
     * @param nomEntreprisePC
     * @param sirenEntreprisePC
     * @param codeAssurancePC
     * @param nomAssurancePC
     * @param niveau
     * @param intituleFormation
     * @return preconvention crée
     */
    public Preconvention creerPreconvention(String nomE, String prenomE, int numeroE, Calendar dateDebutPC, Calendar dateFinPC, 
            float gratificationPC, String resumePC, String nomEntreprisePC, 
            int sirenEntreprisePC, int codeAssurancePC, String nomAssurancePC,
            String niveau, String intituleFormation) {
        // Création de l'étudiant
        // C'est une maquette, on ne cherche pas pour le moment à vérifier si l'étudiant existe, pas de BDD
        Etudiant etu = etudiants.creeEtudiant(numeroE, nomE, prenomE);
        
        // Génération d'un id (numéro de dossier)
        int id = (int) Math.round(Math.random()*1000);
        
        Preconvention pc = new Preconvention(id, etu, dateDebutPC, 
                dateFinPC, gratificationPC, resumePC, EtatPreconvention.INIT, nomEntreprisePC, sirenEntreprisePC, codeAssurancePC, nomAssurancePC, niveau, intituleFormation);
        preconventions.add(pc);
        envoyerAValidation(pc);
        return pc;
    }

    /**
     * Envoi la préconvention dans le topic JMS
     * @param pc
     * @return preconvention envoyée
     */
    public Preconvention envoyerAValidation(Preconvention pc) {
        try {
            ObjectMessage om = context.createObjectMessage(pc);
            om.setJMSType(pc.getEtatPC().toString());
            context.createProducer().send(topic, om);
            return pc;
        } catch (JMSException ex) {
            Logger.getLogger(PreconventionsSingleton.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /**
     * Méthode apellée à chaque réception JMS d'une préconvention
     * Met à jour la liste en mémoire
     * @param newPc 
     */
    public void miseAJourPreconvention(Preconvention newPc) {
        Preconvention preFind = this.rechercher(newPc.getNoPC());
                
        if (preFind != null) {
            // On remplace l'object car plusieurs attributs
            // peuvent être modifiés, on veux la dernière version
            preconventions.set(preconventions.indexOf(preFind), newPc);
        } else {
            preconventions.add(newPc);
        }
    }
    
    /**
     * Rechercher une préconvention par id dans la liste en mémoire
     * @param noPc
     * @return Préconvention ou null
     */
    private Preconvention rechercher(int noPc) {
        for (Preconvention p: this.preconventions) {
            if (p.getNoPC() == noPc) {
                return p;
            }
        }
        
        return null;
    }
}